import sys
import re


class Dot():
    def __init__(self, x, y):
        self.col = int(x)
        self.row = int(y)

    def __str__(self):
        return f'[x={self.x}, y={self.y}]'


class Paper():
    def __init__(self, dots):
        self.col_max = max(dots, key=lambda d: d.col).col + 1
        self.row_max = max(dots, key=lambda d: d.row).row + 1
        self.state = [[0] * (self.col_max) for i in range(self.row_max)]
        for dot in dots:
            self.state[dot.row][dot.col] = 1

    def fold(self, instruction):
        if instruction['axis'] == 'x':
            self.x_fold(instruction['value'])
        else:
            self.y_fold(instruction['value'])

    def y_fold(self, index):
        new_state = self.state[:index][:]
        folded_state = self.state[(index + 1):][:]
        folded_state_len = len(folded_state)

        for row in range(folded_state_len):
            for col in range(self.col_max):
                if folded_state[row][col] == 1:
                    new_state[(index - 1) - row][col] = 1

        self.state = new_state
        self.row_max = index

    def x_fold(self, index):
        transposed_state = list(map(list, zip(*self.state)))
        new_state = transposed_state[:index][:]
        folded_state = transposed_state[(index + 1):][:]
        folded_state_len = len(folded_state)

        for row in range(folded_state_len):
            for col in range(self.row_max):
                if folded_state[row][col] == 1:
                    new_state[(index - 1) - row][col] = 1

        self.state = list(map(list, zip(*new_state)))
        self.col_max = index

    def count_visible(self):
        count = 0
        for row in range(self.row_max):
            for col in range(self.col_max):
                if self.state[row][col]:
                    count += 1

        return count

    def __str__(self):
        string = ''
        for row in range(self.row_max):
            for col in range(self.col_max):
                if self.state[row][col]:
                    string += '#'
                else:
                    string += '.'
            string += '\n'

        return string


def main():
    dots = []
    instructions = []

    state = 'dots'
    for line in sys.stdin:
        line = line.strip()

        if state == 'dots':
            if line == '':
                state = 'instructions'
            else:
                dot_pos = line.split(',')
                dots.append(Dot(*dot_pos))

        if state == 'instructions':
            string = re.search(r'[xy]=\d*', line)
            if string:
                string = string.group(0)
                instructions.append({'axis': string[0],
                                     'value': int(string[2:])})

    paper = Paper(dots)

    print('Before folding: ')
    # print(paper)
    print(f'Number of dots visible: {paper.count_visible()}')

    for instr in instructions:
        paper.fold(instr)
        print(f'\nAfter fold {instr}:')
        # print(paper)
        print(f'Number of dots visible: {paper.count_visible()}')

    print('\nResult: ')
    print(paper)


if __name__ == '__main__':
    main()
