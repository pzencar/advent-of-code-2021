import re
import sys


class Point():
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __str__(self):
        string = ('x = %s, y = %s' % (self.x, self.y))
        return string


class Line():
    def __init__(self, point1, point2):
        self.no_diag = None
        self.start = point1
        self.end = point2
        self.points = self._connect_points()
        # print('\n')
        # print('Starting point: %s' % self.start)
        # print('Ending point: %s' % self.end)
        # [print(point) for point in self.points]

    def _connect_points(self):
        points = []
        if all(((self.start.x != self.end.x),
                (self.start.y != self.end.y))):
            self.no_diag = False
        else:
            self.no_diag = True

        if self.start.x == self.end.x:
            if self.start.y > self.end.y:
                for i in range(self.start.y - self.end.y + 1):
                    y = self.start.y - i
                    points.append(Point(self.start.x, y))
            elif self.start.y < self.end.y:
                for i in range(self.end.y - self.start.y + 1):
                    y = self.start.y + i
                    points.append(Point(self.start.x, y))
        elif self.start.y == self.end.y:
            if self.start.x > self.end.x:
                for i in range(self.start.x - self.end.x + 1):
                    x = self.start.x - i
                    points.append(Point(x, self.start.y))
            elif self.start.x < self.end.x:
                for i in range(self.end.x - self.start.x + 1):
                    x = self.start.x + i
                    points.append(Point(x, self.start.y))
        else:
            if self.start.x > self.end.x and self.start.y > self.end.y:
                for i in range(self.start.x - self.end.x + 1):
                    x = self.start.x - i
                    y = self.start.y - i
                    points.append(Point(x, y))
            elif self.start.x < self.end.x and self.start.y < self.end.y:
                for i in range(self.end.x - self.start.x + 1):
                    x = self.start.x + i
                    y = self.start.y + i
                    points.append(Point(x, y))
            elif self.start.x < self.end.x and self.start.y > self.end.y:
                for i in range(self.end.x - self.start.x + 1):
                    x = self.start.x + i
                    y = self.start.y - i
                    points.append(Point(x, y))
            elif self.start.x > self.end.x and self.start.y < self.end.y:
                for i in range(self.start.x - self.end.x + 1):
                    x = self.start.x - i
                    y = self.start.y + i
                    points.append(Point(x, y))

        return points


def main():
    input_l = []
    for line in sys.stdin:
        input_l.append(line.strip())

    input_l = [string_line.split('->') for string_line in input_l]
    for i, point_pair in enumerate(input_l):
        point_pair = [list(map(int, point.split(','))) for point in point_pair]
        input_l[i] = point_pair

    x_list = []
    y_list = []

    for point_pair in input_l:
        x_list.append(point_pair[0][0])
        x_list.append(point_pair[1][0])
        y_list.append(point_pair[0][1])
        y_list.append(point_pair[1][1])

    x_max = max(x_list) + 1
    y_max = max(y_list) + 1

    board_no_diag = [[0] * x_max for i in range(y_max)]
    board = [[0] * x_max for i in range(y_max)]
    for points in input_l:
        point1 = Point(points[0][0], points[0][1])
        point2 = Point(points[1][0], points[1][1])
        line = Line(point1, point2)
        for point in line.points:
            board[point.x][point.y] += 1
            if line.no_diag:
                board_no_diag[point.x][point.y] += 1

    total_count = 0
    total_count_no_diag = 0
    for x_idx in range(len(board)):
        for y_idx in range(len(board[0])):
            if board[x_idx][y_idx] >= 2:
                total_count += 1
            if board_no_diag[x_idx][y_idx] >= 2:
                total_count_no_diag += 1

    print('Total count NOT considering diagonals: %s' % total_count_no_diag)
    print('Total count considering diagonals: %s' % total_count)


if __name__ == '__main__':
    main()
