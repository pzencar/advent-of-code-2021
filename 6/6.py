import sys
import numpy as np
import re
import time


def main():
    input_l = input()

    input_l = list(map(int, input_l.split(',')))

    generations = np.array([0] * 9, dtype='uint64')

    for fish in input_l:
        generations[fish] += 1

    print(generations)

    no_of_days = 256

    for day_number in range(no_of_days):
        generations = np.roll(generations, -1)
        generations[6] += generations[8]
        if day_number == 80:
            print('Day 80, number of fishes: %s' % np.sum(generations))

    print('Day %s, number of fishes: %s' % (no_of_days, np.sum(generations)))


if __name__ == '__main__':
    main()
