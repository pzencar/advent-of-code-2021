import sys

input_l = []
for line in sys.stdin:
    line_l = line.split(' ')
    line_l[1] = int(line_l[1])
    input_l.append(line_l)

horiz = 0
depth = 0

for command in input_l:
    if command[0] == 'forward':
        horiz += command[1]
    elif command[0] == 'up':
        depth -= command[1]
    elif command[0] == 'down':
        depth += command[1]

print('Part one')
print(f'Horizontal distance: {horiz}')
print(f'Depth: {depth}')
print(f'Multiplicaction: {horiz * depth}')

horiz = 0
depth = 0
aim = 0
for command in input_l:
    if command[0] == 'forward':
        horiz += command[1]
        depth += aim * command[1]
    elif command[0] == 'up':
        aim -= command[1]
    elif command[0] == 'down':
        aim += command[1]

print('\nPart two')
print(f'Horizontal distance: {horiz}')
print(f'Depth: {depth}')
print(f'Multiplicaction: {horiz * depth}')
