import sys
import re


class Board():
    def __init__(self, board):
        self.board = board
        self.row_max = len(self.board)
        self.col_max = len(self.board)
        self.no_of_flashes = 0

    def age(self):
        for row in range(self.row_max):
            for col in range(self.col_max):
                self.board[row][col] += 1

    def _get_current_flashing(self, flashing, already_flashed):
        new_flashing = []

        for row in range(self.row_max):
            for col in range(self.col_max):
                if all((self.board[row][col] >= 9,
                        [row, col] not in already_flashed,
                        [row, col] not in flashing)):
                    new_flashing.append([row, col])

        return new_flashing

    def _update_board_flash(self, current):
        current_row = current[0]
        current_col = current[1]

        # Left
        row = current_row
        col = current_col - 1
        if current_col != 0:
            self.board[row][col] += 1

        # Left Top
        row = current_row + 1
        col = current_col - 1
        if current_col != 0 and row < self.row_max:
            self.board[row][col] += 1

        # Top
        row = current_row + 1
        col = current_col
        if row < self.row_max:
            self.board[row][col] += 1

        # Right Top
        row = current_row + 1
        col = current_col + 1
        if col < self.col_max and row < self.row_max:
            self.board[row][col] += 1

        # Right
        row = current_row
        col = current_col + 1
        if col < self.col_max:
            self.board[row][col] += 1

        # Right Bottom
        row = current_row - 1
        col = current_col + 1
        if col < self.col_max and current_row != 0:
            self.board[row][col] += 1

        # Bottom
        row = current_row - 1
        col = current_col
        if current_row != 0:
            self.board[row][col] += 1

        # Left Bottom
        row = current_row - 1
        col = current_col - 1
        if current_col != 0 and current_row != 0:
            self.board[row][col] += 1

    def flash(self):
        already_flashed = []
        flashing = []
        flashing.extend(self._get_current_flashing(flashing, already_flashed))

        while flashing:
            current = flashing.pop(0)
            self._update_board_flash(current)
            already_flashed.append(current)
            flashing.extend(self._get_current_flashing(flashing,
                                                       already_flashed))

        for coords in already_flashed:
            self.board[coords[0]][coords[1]] = -1

        self.no_of_flashes += len(already_flashed)

    def is_zero(self):
        is_zero = True

        for row in range(self.row_max):
            for col in range(self.col_max):
                if self.board[row][col] != 0:
                    is_zero = False
                    break

        return is_zero

    def __str__(self):
        string = ''

        for row in range(self.row_max):
            for col in range(self.col_max):
                string += str(self.board[row][col])
            string += '\n'

        return string


def main():
    board = []
    for line in sys.stdin:
        row = line.strip()
        row = list(map(int, row))
        board.append(row)

    board = Board(board)

    no_of_days = 250 + 1

    synchro_found = False
    for day in range(1, no_of_days + 1):
        print(f'\nDay {day}\n{board}')
        print(f'Total number of flashes: {board.no_of_flashes}')
        board.flash()
        board.age()
        if board.is_zero():
            synchro_found = True
            print(f'First synchronization occured at day {day}')
            break

    if not synchro_found:
        print(f'''Synchronization did not happen in {day} days.
                  Try increasing number of days''')


if __name__ == '__main__':
    main()
