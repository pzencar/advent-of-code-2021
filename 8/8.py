import sys


def get_missing_letter(source, string):
    for letter in source:
        if letter not in string:
            return letter


def decode(patterns):
    segments = {'A': '',
                'B': '',
                'C': '',
                'D': '',
                'E': '',
                'F': '',
                'G': ''}
    # Find B C
    for value in patterns:
        if len(value) == 2:
            segments['B'] = value
            segments['C'] = value

    # Find A
    for value in patterns:
        if len(value) == 3:
            for letter in value:
                if letter not in segments['B']:
                    segments['A'] = letter

    # Find F G
    for value in patterns:
        if len(value) == 4:
            for letter in value:
                if letter not in segments['B']:
                    segments['F'] += letter
                    segments['G'] += letter

    # Find E
    len_six = []
    for value in patterns:
        if len(value) == 6:
            len_six.append(value)

    for value in len_six:
        success = True
        for letter in segments['F']:
            if letter not in value:
                success = False

        for letter in segments['B']:
            if letter not in value:
                success = False

        if success:
            segments['E'] = get_missing_letter('abcdefg', value)

    # Find B C F G
    len_five = []
    for value in patterns:
        if len(value) == 5:
            len_five.append(value)

    for value in len_five:
        if segments['E'] not in value:
            source = 'abcdefg'.replace(segments['E'], '')
            missing_letter = get_missing_letter(source, value)
            if missing_letter in segments['B']:
                segments['B'] = missing_letter
                segments['C'] = segments['C'].replace(missing_letter, '')
            elif missing_letter in segments['F']:
                segments['F'] = missing_letter
                segments['G'] = segments['G'].replace(missing_letter, '')

    # Find D
    found_so_far = ''
    for key, item in segments.items():
        if item != '':
            found_so_far += item

    segments['D'] = get_missing_letter('abcdefg', found_so_far)

    return segments


def decode_output(outputs, segments):
    digits = []
    for output in outputs:
        segment_code = get_real_segments(output, segments)
        digits.append(decode_segment_code(segment_code))

    int_val = int(''.join([str(digit) for digit in digits]))
    return int_val


def get_real_segments(output, segments):
    segment_code = ''
    for key, item in segments.items():
        if item in output:
            segment_code += key

    return segment_code


def decode_segment_code(segment_code):
    if segment_code == 'ABCDEF':
        return 0
    elif segment_code == 'BC':
        return 1
    elif segment_code == 'ABDEG':
        return 2
    elif segment_code == 'ABCDG':
        return 3
    elif segment_code == 'BCFG':
        return 4
    elif segment_code == 'ACDFG':
        return 5
    elif segment_code == 'ACDEFG':
        return 6
    elif segment_code == 'ABC':
        return 7
    elif segment_code == 'ABCDEFG':
        return 8
    elif segment_code == 'ABCDFG':
        return 9



def main():
    input_l = []
    for line in sys.stdin:
        line = line.strip().split('|')
        signal_patters = line[0].split()
        output_value = line[1].split()
        read_line = {'patterns': signal_patters, 'output': output_value}
        input_l.append(read_line)

    # Part 1
    uique_lengths = [2, 4, 3, 7]

    output_unique_len_count = 0

    for line in input_l:
        for output_val in line['output']:
            if len(output_val) in uique_lengths:
                output_unique_len_count += 1

    print('Part 1')
    print('Unique output digit length: %s' % output_unique_len_count)

    # Part 2
    line_ouputs = []
    for line in input_l:

        patterns = line['patterns']

        segments = decode(patterns)

        outputs = line['output']

        int_output = decode_output(outputs, segments)

        line_ouputs.append(int_output)

    print('\nPart 2')
    print('Sum of all outputs is %s' % sum(line_ouputs))


if __name__ == '__main__':
    main()
