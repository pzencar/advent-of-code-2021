import sys
import math


class Expansion():
    def __init__(self, board, origin):
        self.origin = origin
        self.board = board
        self.col_max = len(board[0])
        self.row_max = len(board)
        self.points = []

        self.expand(self.origin)

    def expand(self, point):
        self.points.append(point)
        # right side
        row = point[0]
        col = point[1] + 1
        if col < self.col_max:
            if [row, col] not in self.points and self.board[row][col] != 9:
                self.expand([row, col])

        # # right top side
        # row = point[0] + 1
        # col = point[1] + 1
        # if row < self.row_max and col < self.col_max:
        #     if [row, col] not in self.points and self.board[row][col] != 9:
        #         self.expand([row, col])

        # top side
        row = point[0] + 1
        col = point[1]
        if row < self.row_max:
            if [row, col] not in self.points and self.board[row][col] != 9:
                self.expand([row, col])

        # # left top side
        # row = point[0] + 1
        # col = point[1] - 1
        # if row < self.row_max and col >= 0:
        #     if [row, col] not in self.points and self.board[row][col] != 9:
        #         self.expand([row, col])

        # left side
        row = point[0]
        col = point[1] - 1
        if col >= 0:
            if [row, col] not in self.points and self.board[row][col] != 9:
                self.expand([row, col])

        # # left down side
        # row = point[0] - 1
        # col = point[1] - 1
        # if row >= 0 and col >= 0:
        #     if [row, col] not in self.points and self.board[row][col] != 9:
        #         self.expand([row, col])

        # bottom side
        row = point[0] - 1
        col = point[1]
        if row >= 0:
            if [row, col] not in self.points and self.board[row][col] != 9:
                self.expand([row, col])

        # # right down side
        # row = point[0] - 1
        # col = point[1] + 1
        # if row >= 0 and col < self.col_max:
        #     if [row, col] not in self.points and self.board[row][col] != 9:
        #         self.expand([row, col])


def main():
    board = []
    for line in sys.stdin:
        line = line.strip()
        line = [int(num) for num in line]
        board.append(line)

    width = len(board[0])
    height = len(board)

    low_points = []
    for idx_row in range(height):
        for idx_col in range(width):
            is_min = True
            val = board[idx_row][idx_col]

            if idx_row != 0:
                if board[idx_row - 1][idx_col] <= val:
                    is_min = False
            if idx_row != (height - 1):
                if board[idx_row + 1][idx_col] <= val:
                    is_min = False
            if idx_col != 0:
                if board[idx_row][idx_col - 1] <= val:
                    is_min = False
            if idx_col != (width - 1):
                if board[idx_row][idx_col + 1] <= val:
                    is_min = False

            if is_min:
                low_points.append({'val': val, 'row': idx_row, 'col': idx_col})

    risk_factor = [(1 + val['val']) for val in low_points]
    print(f'Sum of risk factor: {sum(risk_factor)}')

    for low_point in low_points:
        expansion = Expansion(board, [low_point['row'], low_point['col']])
        low_point['size'] = len(expansion.points)

    sorted_low_points = sorted(low_points,
                               key=lambda point: point['size'],
                               reverse=True)
    largest_sizes = [lp['size'] for lp in sorted_low_points[:3]]
    print(f'''Sizes of low point areas multiplied are {math.prod(largest_sizes)}''')


if __name__ == '__main__':
    main()
