import sys


class Board():
    def __init__(self, board_list, board_size):
        self.board = board_list
        self.size = board_size
        self.mark = [[0] * self.size for i in range(self.size)]
        self.score = None

    def draw_number(self, number):
        for i_row in range(self.size):
            for i_col in range(self.size):
                if self.board[i_row][i_col] == number:
                    self.mark[i_row][i_col] = 1

        if self.check_win():
            self.evaluate_result(number)
            return True

        return False

    def check_win(self):
        row_sum = [sum(row) for row in self.mark]
        try:
            row_sum.index(self.size)
            return True
        except ValueError:
            pass

        mark_columnwise = list(map(list, zip(*self.mark)))
        col_sum = [sum(col) for col in mark_columnwise]
        try:
            col_sum.index(self.size)
            return True
        except ValueError:
            pass

        return False

    def evaluate_result(self, number):
        unmarked = []
        for i_row in range(self.size):
            for i_col in range(self.size):
                if self.mark[i_row][i_col] == 0:
                    unmarked.append(self.board[i_row][i_col])

        unmarked_sum = sum(unmarked)
        self.score = unmarked_sum * number


input_l = []
for line in sys.stdin:
    input_l.append(line)

input_l = [string[:-1] for string in input_l if string != '\n']
input_l[0] = input_l[0].split(',')
input_l[1:] = [string.split() for string in input_l[1:]]
for index, string_list in enumerate(input_l):
    input_l[index] = [int(string) for string in string_list]

boards = []
board_size = 5
board_count = int((len(input_l) - 1) / board_size)
for i in range(board_count):
    board = input_l[1 + i * board_size: 1 + board_size + i * board_size]
    boards.append(Board(board, board_size))

numbers_drawn = input_l[0]

first_win_board = None
last_win_board = None
for number in numbers_drawn:
    for board in boards:
        if board.draw_number(number):
            if first_win_board is None:
                first_win_board = board
            last_win_board = board
            # Remove winning board from boards list
            boards = [brd for brd in boards if brd is not board]

print(f'Result of first winning board: {first_win_board.score}')
print(f'Result of last winning board: {last_win_board.score}')
