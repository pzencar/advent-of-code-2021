import sys

def most_common_bit_val(bit_list):
    summed = sum(bit_list)
    if summed >= len(bit_list) / 2:
        return 1
    else:
        return 0


input_l = []
for line in sys.stdin:
    input_l.append(line)

# input_l = ['00100\n',
#            '11110\n',
#            '10110\n',
#            '10111\n',
#            '10101\n',
#            '01111\n',
#            '00111\n',
#            '11100\n', '10000\n', '11001\n', '00010\n', '01010\n']

input_l = [[int(inner_val) for inner_val in list(val[:-1])] for val in input_l]

# Part 1

bit_wise_input = list(map(list, zip(*input_l)))
gamma = [most_common_bit_val(bit_list) for bit_list in bit_wise_input]

mask = ['1' for val in gamma]
mask = int(''.join(mask), 2)

gamma = [str(val) for val in gamma]
gamma = int(''.join(gamma), 2)

epsilon = gamma ^ mask

print(f'\nPart one')
print(f'{gamma = }')
print(f'{epsilon = }')
print(f'{gamma * epsilon = }')

# Part 2

# Oxygen rating
oxygen_rating = 0
reduced_input = input_l
for i in range(len(input_l[0])):
    bit_wise_input = list(map(list, zip(*reduced_input)))
    most_com = most_common_bit_val(bit_wise_input[i])
    reduced_input = [val for val in reduced_input if val[i] != (most_com ^ 1)]
    if len(reduced_input) == 1:
        oxygen_rating = [str(val) for val in reduced_input[0]]
        oxygen_rating = int(''.join(oxygen_rating), 2)
        break

# CO2 rating
co2_rating = 0
reduced_input = input_l
for i in range(len(input_l[0])):
    bit_wise_input = list(map(list, zip(*reduced_input)))
    most_com = most_common_bit_val(bit_wise_input[i])
    reduced_input = [val for val in reduced_input if val[i] != most_com]
    if len(reduced_input) == 1:
        co2_rating = [str(val) for val in reduced_input[0]]
        co2_rating = int(''.join(co2_rating), 2)
        break

print(f'\nPart two')
print(f'{oxygen_rating = }')
print(f'{co2_rating = }')
print(f'{oxygen_rating * co2_rating = }')
