import sys

input_l = []
for line in sys.stdin:
    input_l.append(int(line))
# with open('1_input.txt', 'r') as file:
#     while True:
#         try:
#             input_l.append(int(file.readline()))
#         except ValueError:
#             break

rising = 0
for i, item in enumerate(input_l):
    if i != 0:
        if input_l[i] > input_l[i - 1]:
            rising += 1


print(f'Output for first part: {rising}')

input_summed = []
for i, item in enumerate(input_l):
    if i < len(input_l) - 1 - 1:
        input_summed.append(input_l[i]
                            + input_l[i + 1]
                            + input_l[i + 2]
                            )

rising = 0
for i, item in enumerate(input_summed):
    if i != 0:
        if input_summed[i] > input_summed[i - 1]:
            rising += 1

print(f'Output for first part: {rising}')
