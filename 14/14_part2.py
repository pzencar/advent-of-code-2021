import sys
import re
import time
from collections import Counter


def main():
    polymers = {}
    replacement_dict = {}
    template = ''
    for line in sys.stdin:
        line = line.strip()
        if '->' not in line:
            if line != '':
                template = line
        else:
            polymer = line.split(' -> ')
            polymers[polymer[0]] = polymer[1]
            replacement_dict[polymer[0]] = [polymer[0][0] + polymer[1],
                                            polymer[1] + polymer[0][1]]

    main_counter = Counter()
    for i in range(1, len(template)):
        key = template[i - 1:i + 1]
        main_counter[key] += 1

    no_of_steps = 40

    print('step 0')
    for step in range(1, no_of_steps + 1):
        print(f'{step = }')
        new_counter = main_counter.copy()
        for key in main_counter:
            if key in replacement_dict:
                new_counter[key] -= main_counter[key]
                if new_counter[key] == 0:
                    del new_counter[key]
                new_counter[replacement_dict[key][0]] += main_counter[key]
                new_counter[replacement_dict[key][1]] += main_counter[key]
        main_counter = new_counter

        char_counter = Counter()
        for key in main_counter:
            char_counter[key[0]] += main_counter[key]
            char_counter[key[1]] += main_counter[key]

        for key in char_counter:
            char_counter[key] //= 2

        char_counter[template[0]] += 1
        char_counter[template[-1]] += 1

        most_common_key, most_common_cnt = char_counter.most_common()[0]
        least_common_key, least_common_cnt = char_counter.most_common()[-1]

        print(f'Max quantity: Element: {(most_common_key, most_common_cnt)}')
        print(f'Min quantity: Element: {(least_common_key, least_common_cnt)}')
        difference = most_common_cnt - least_common_cnt
        print(f'Difference is {difference}.')


if __name__ == '__main__':
    main()
