import sys
import re
import time


def main():
    polymers = {}
    replacement_dict = {}
    template = ''
    for line in sys.stdin:
        line = line.strip()
        if '->' not in line:
            if line != '':
                template = line
        else:
            polymer = line.split(' -> ')
            polymers[polymer[0]] = polymer[1]
            repl = polymer[0][0] + polymer[1] + polymer[0][1]
            replacement_dict[polymer[0]] = repl

    polymer_pattern_string = r'(?=('
    for name in polymers:
        polymer_pattern_string += r'%s|' % name

    polymer_pattern_string = polymer_pattern_string[:-1]
    polymer_pattern_string += r'))'
    polymer_pattern = re.compile(polymer_pattern_string)

    no_of_steps = 10

    print('step 0')
    print(f'{len(template) = }\n')
    for step in range(1, no_of_steps + 1):
        print(f'{step = }')
        matches = polymer_pattern.finditer(template)
        st_t = time.time()
        last_index = 0
        template_new = ''
        for i, match in enumerate(matches):
            start_index = match.start(1)
            end_index = match.end(1)
            template_new += template[last_index:start_index]
            if start_index >= last_index:
                template_new += replacement_dict[match.group(1)]
            else:
                template_new += replacement_dict[match.group(1)][1:]
            last_index = end_index + 1

        template = template_new

        print(f'Replace time: {time.time() - st_t}')
        print(f'{len(template) = }\n')

    quantity_table = []
    for key, name in polymers.items():
        quantity = len(re.findall(r'%s' % name, template))
        dict_to_append = {'name': name, 'quantity': quantity}
        if dict_to_append not in quantity_table:
            quantity_table.append(dict_to_append)

    quantity_table = sorted(quantity_table,
                            key=lambda q: q['quantity'],
                            reverse=True)

    print(f'Max quantity: Element: {quantity_table[0]}, ')
    print(f'Min quantity: Element: {quantity_table[-1]}, ')
    difference = quantity_table[0]["quantity"] - quantity_table[-1]["quantity"]
    print(f'Difference is {difference}.')


if __name__ == '__main__':
    main()
