import sys
import re


def is_closing(bracket):
    match = re.search(r'%s' % ('\\' + bracket), '])}>')
    if match is not None:
        return True
    else:
        return False


def get_pair(bracket):
    pair_bracket = None

    if bracket == '(':
        pair_bracket = ')'
    elif bracket == ')':
        pair_bracket = '('
    elif bracket == '[':
        pair_bracket = ']'
    elif bracket == ']':
        pair_bracket = '['
    elif bracket == '<':
        pair_bracket = '>'
    elif bracket == '>':
        pair_bracket = '<'
    elif bracket == '{':
        pair_bracket = '}'
    elif bracket == '}':
        pair_bracket = '{'

    return pair_bracket


def evaluate_missing_links(links, eval_dict):
    score = 0
    for bracket in links:
        score *= 5
        score += eval_dict[bracket]

    return score


def main():

    eval_dict_syntax_checker = {')': 3, ']': 57, '}': 1197, '>': 25137}
    eval_dict_autocomplete = {')': 1, ']': 2, '}': 3, '>': 4}

    lines = []
    for line in sys.stdin:
        lines.append(line.strip())

    line_errors = []
    incomplete_lines = []
    for line in lines:
        active = []
        corrupted = False
        for bracket in line:
            if not is_closing(bracket):
                active.append(bracket)
            else:
                valid = active.pop()
                if bracket != get_pair(valid):
                    line_errors.append(bracket)
                    corrupted = True
                    break
        if not corrupted:
            incomplete_lines.append(active)

    illegal_score = 0
    for error in line_errors:
        illegal_score += eval_dict_syntax_checker[error]

    print(f'Total illegal score is {illegal_score}')

    missing_links = []
    for line in incomplete_lines:
        size = len(line)
        # incomplete lines are destroyed
        missing_links.append([get_pair(line.pop()) for count in range(size)])

    autocomplete_scores = []
    for link in missing_links:
        score = evaluate_missing_links(link, eval_dict_autocomplete)
        autocomplete_scores.append(score)

    autocomplete_scores = sorted(autocomplete_scores)
    final_autoc_score = autocomplete_scores[int(len(autocomplete_scores) / 2)]

    print(f'Final autocomplete score: {final_autoc_score}')


if __name__ == '__main__':
    main()
