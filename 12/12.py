import sys
import copy

class Cave():
    def __init__(self, name):
        self.name = name
        if name == 'start':
            self.is_start = True
            self.is_end = False
            self.is_small = False
        elif name == 'end':
            self.is_start = False
            self.is_end = True
            self.is_small = False
        elif name.isupper():
            self.is_start = False
            self.is_end = False
            self.is_small = False
        elif not name.isupper():
            self.is_start = False
            self.is_end = False
            self.is_small = True

        self.connected_to = []

    def connect(self, cave):
        if cave not in self.connected_to:
            self.connected_to.append(cave)

    def propagate_with_extra_visit(self, all_paths, input_path):
        path = copy.deepcopy(input_path)
        path.append(self.name)
        if self.is_end:
            all_paths.append(path)
        else:
            for cave in self.connected_to:
                if not any((input_path[:-1] == cave.name,
                            (cave.is_small
                             and cave._is_small_forbidden(path, cave.name)),
                            cave.is_start)):
                    cave.propagate_with_extra_visit(all_paths, path)

    def propagate(self, all_paths, input_path):
        path = copy.deepcopy(input_path)
        path.append(self.name)
        if self.is_end:
            all_paths.append(path)
        else:
            for cave in self.connected_to:
                if not any((input_path[:-1] == cave.name,
                            cave.is_small and cave.name in path,
                            cave.is_start)):
                    cave.propagate(all_paths, path)

    def _is_small_forbidden(self, path, name):
        forbidden = None
        extra_visit_allowed = True
        for cave in path:
            if not cave.isupper():
                no_of_visits = len(self._indices(path, cave))
                if no_of_visits >= 2:
                    extra_visit_allowed = False
                    break

        if extra_visit_allowed:
            no_of_visits = len(self._indices(path, name))
            if no_of_visits > 1:
                forbidden = True
        else:
            no_of_visits = len(self._indices(path, name))
            if no_of_visits > 0:
                forbidden = True

        return forbidden

    def _indices(self, path, name):
        indices = []
        for i, val in enumerate(path):
            if val == name:
                indices.append(i)

        return indices

    def __str__(self):
        string = f'\nCave {self.name}\n'
        string += f'{self.is_small = }\n'
        string += f'{self.is_start = }\n'
        string += f'{self.is_end = }\n'
        string += f'self.connected_to: ['
        for i, cave in enumerate(self.connected_to):
            string += f'{cave.name}'
            if i != len(self.connected_to) - 1:
                string += ', '
        string += ']\n'

        return string


def main():
    connections = []
    for line in sys.stdin:
        con = line.strip()
        con = con.split('-')
        connections.append(con)

    caves = {}
    for con in connections:
        caves[con[0]] = Cave(con[0])
        caves[con[1]] = Cave(con[1])

    for con in connections:
        caves[con[0]].connect(caves[con[1]])
        caves[con[1]].connect(caves[con[0]])

    [print(cave) for key, cave in caves.items()]

    all_paths = []
    input_path = []
    caves['start'].propagate(all_paths, input_path)

    print('Paths without extra visit: ')
    [print(path) for path in all_paths]
    print(f'\nNumber of possible paths: {len(all_paths)}')

    all_paths = []
    input_path = []
    caves['start'].propagate_with_extra_visit(all_paths, input_path)

    print('\nPaths with extra visit: ')
    [print(path) for path in all_paths]
    print(f'\nNumber of possible paths: {len(all_paths)}')


if __name__ == '__main__':
    main()
