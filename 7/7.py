
class DistanceGetter():
    def __init__(self, origin):
        self.origin = origin

    def get_dist(self, point):
        return abs(self.origin - point)

    def get_dist_incr(self, point):
        fuel_consumed = 0
        fuel_cost = 1
        for i in range(abs(self.origin - point)):
            fuel_consumed += fuel_cost
            fuel_cost += 1

        return fuel_consumed


def main():
    input_l = input()

    input_l = list(map(int, input_l.split(',')))

    min_pos = min(input_l)
    max_pos = max(input_l)

    total_fuels_part1 = []
    total_fuels_part2 = []
    for position in range(min_pos, max_pos + 1):
        distances_part1 = list(map(DistanceGetter(position).get_dist, input_l))
        total_fuels_part1.append({'position': position,
                                  'total_fuel': sum(distances_part1)})

        distances_part2 = list(map(DistanceGetter(position).get_dist_incr,
                                   input_l))
        total_fuels_part2.append({'position': position,
                                  'total_fuel': sum(distances_part2)})

    sorted_total_fuels_part1 = sorted(total_fuels_part1,
                                      key=lambda tf: tf['total_fuel'])

    sorted_total_fuels_part2 = sorted(total_fuels_part2,
                                      key=lambda tf: tf['total_fuel'])

    print('\nPart 1')
    print(f'Least fuel in {sorted_total_fuels_part1[0]}')

    print('\nPart 2')
    print(f'Least fuel in {sorted_total_fuels_part2[0]}')


if __name__ == '__main__':
    main()
